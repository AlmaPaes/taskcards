# Práctica React
Tecnologías para el Desarrollo en Internet 2021-1

## Autora
Alma Rosa Páes Alcalá

## Instalaciones necesarias
**Para usar archivos json como back-end**
- `npm install -g json-server`

**Para ver los íconos**
- `npm i --save @fortawesome/fontawesome-svg-core`
- `npm install --save @fortawesome/free-solid-svg-icons`
- `npm install --save @fortawesome/react-fontawesome`

## Cómo ejecutar

### Servidor para simular un back con archivos json
- Ejecutar en terminal `json-server --watch src/todos.json --port 3005`

### Ejecutar aplicación
- Ejecutar en otra terminal `npm start`

