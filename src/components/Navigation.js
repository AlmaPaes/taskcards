import { Component }from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTasks } from '@fortawesome/free-solid-svg-icons'

import '../App.css';

class Navigation extends Component {
    

    render() {
        return(
          <div className="App-header">
            <h1 className="titulo"><FontAwesomeIcon icon={faTasks} />&nbsp;&nbsp;{this.props.title}</h1>
            <h2 className="contador">Número de tareas: {this.props.ntareas}</h2> 
          </div>
        );
    }
}

export default Navigation;