import { React,Component } from 'react';
import { Button, Label, Input, Form, FormGroup, Modal, ModalHeader, ModalBody} from 'reactstrap';


class TareaForm extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            responsable: "",
            descripcion: "",
            prioridad: 'low'
        }
    }

    hideModal = () => {
        this.props.onHide();
    }

    getMax = () => {
        return this.props.maximo;
    }

    createTarea = () => {
        fetch("http://localhost:3005/todos", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify( {
                id: this.getMax() + 1,
                title: this.state.title,
                responsable: this.state.responsable,
                descripcion: this.state.descripcion,
                prioridad: this.state.prioridad
            })
        }).then(
            this.props.onAdd()
        );
    }

    resetForm = () => {
        this.setState({
            title: '',
            responsable: '',
            descripcion: '',
            prioridad: 'low'
        });
    }

    handleSubmit = (event) => {
        this.createTarea();
        this.hideModal();
        this.resetForm();
        event.preventDefault();
    }

    handleTitle = (event) => {
        this.setState({
            title: event.target.value
        })
    }

    handleResponsable = (event) => {
        this.setState({
            responsable: event.target.value
        })
    }

    handleDescripcion = (event) => {
        this.setState({
            descripcion: event.target.value
        })
    }

    handlePrioridad = (event) => {
        this.setState({
            prioridad: event.target.value
        })
    }
    
    render() {
        return(
      <Modal id="tareaForm" className="modal-dialog modal-dialog-centered" isOpen={this.props.show}>
        <ModalHeader >
        Agregar Tarea
        </ModalHeader>
        <ModalBody> 
            <Form onSubmit={this.handleSubmit}>
                <FormGroup>
                    <Label for="tareaTitle">Título de la tarea</Label>
                    <Input type="text" value={this.state.title} onChange={this.handleTitle} placeholder="Titulo" />
                </FormGroup>
                &nbsp;
                <FormGroup>
                    <Label for="tareaResponsible">Responsable de la tarea</Label>
                    <Input type="text" value={this.state.responsable} onChange={this.handleResponsable} placeholder="Responsable" />
                </FormGroup>
                &nbsp;
                <FormGroup>
                    <Label for="tareaDescription">Descripcion de la tarea</Label>
                    <Input type="text" value={this.state.descripcion} onChange={this.handleDescripcion} placeholder="Descripcion" />
                </FormGroup>
                &nbsp;
                <FormGroup>
                    <Label for="tareaPriority">Prioridad de la tarea</Label>
                    <Input type="select" value={this.state.prioridad} onChange={this.handlePrioridad} >
                        <option value="low">low</option>
                        <option value="medium" >medium</option>
                        <option value="high">high</option>
                    </Input>
                </FormGroup>
                <br />
                <div className="addTarea-buttons">
                    <Button type="submit" className="btn btn-success" >Agregar</Button> &nbsp; &nbsp;
                    <Button className="btn btn-secondary" onClick={this.hideModal.bind(this)}>Cancel</Button>
                </div>
            </Form>
        </ModalBody>
      </Modal>
        );
    }
}

export default TareaForm;