import { React,Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'

class Tarjeta extends Component {
    constructor() {
        super();
        this.state = {};
        this.removeTarea.bind(this);
    }
    removeTarea(index) {
        this.props.onDelete(index);
    }

    render() {
        return(
            <div className="col-md-4" key={this.props.indice}>
                <div className="card mt-4">
                    <div className="card-header">
                        <h3 className="text-dark"> {this.props.tarea.title} </h3>
                        <div className="data-card">
                            <p>Prioridad: <span className="ml-2 pink-text"> {this.props.tarea.prioridad} </span>
</p>
                        <p>Responsable: <span className="pink-text"> {this.props.tarea.responsable} </span></p>
                        </div>
                    </div>
                    <div className="card-body">
                        <p className="text-dark">{this.props.tarea.descripcion}</p>
                    </div>
                    <div className="card-footer">
                        <button className="btn btn-danger" onClick={this.removeTarea.bind(this, this.props.indice)}><FontAwesomeIcon icon={faTrashAlt} />&nbsp;&nbsp;Delete</button>
                    </div>
                </div>
            </div>
      );
    }
}

export default Tarjeta;