import { React, Component } from 'react';
import './App.css';
import Navigation from './components/Navigation';
import Tarjeta from './components/Tarjeta';
import TareaForm from './components/TareaForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

class App extends Component{
  constructor() {
    super();
    this.state = {
      title: 'Tablero de tareas',
      ntareas: 10,
      showForm: false,
      lastIndex: 0,
      todos: []
    }
    this.getTareas();
  }

  removeTodo = (index) => {
    let indice = this.state.todos[index].id;
    if (window.confirm('Are you sure you want to delete it?')) {
      fetch("http://localhost:3005/todos/" + indice, {
        method: "DELETE"
      })
      .then(res => res.json())
      .then(result => { this.getTareas(); });
    }
    
  }

  setShowForm = (bool) => {
    this.setState({
      showForm: bool
    });
  }

  getMaximum = () => {
    var num = 0;
    for (let t of this.state.todos){
      if (t.id > num){
        num = t.id;
      }
    }

    this.setState({
      lastIndex: num
    })
  }

  getTareas = () => {
    fetch("http://localhost:3005/todos")
    .then(res => res.json())
    .then(result => {
      this.setState({
        todos: result
      });
      this.getMaximum();
    }
    )
    .catch(console.log);
  }


  render(){
    const todos = this.state.todos.map((todo,i) => {
      return (
        <Tarjeta key={i} tarea={todo} indice={i} onDelete={this.removeTodo}></Tarjeta>
      )
    });

    return (
      <div className="App">
        <Navigation title={this.state.title} ntareas={todos.length}></Navigation>
        <div className="container">
            <div className="row mt-4"> {todos}
            <div className="col-md-4 button-space">
            <button className="btn btn-success" onClick={() => this.setShowForm(true)}><FontAwesomeIcon icon={faPlus} />&nbsp;&nbsp;Agregar nueva tarea</button>
            </div>
            </div>
            <TareaForm  maximo={this.state.lastIndex} onAdd={() => this.getTareas} show={this.state.showForm} onHide={() => this.setShowForm(false)}></TareaForm>
        </div>
      </div>
    );
  }
}

export default App;
